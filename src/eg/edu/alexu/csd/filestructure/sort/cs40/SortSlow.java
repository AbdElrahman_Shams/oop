package eg.edu.alexu.csd.filestructure.sort.cs40;

import java.util.ArrayList;

//**********BubbleSort**********//
public class SortSlow <T extends Comparable<T>> {
    public void bubbleSort(ArrayList<T> values) {

        int numOfElements = values.size();

        if (numOfElements == 0 || values == null) {
			return;
		}
        for (int i = 0; i < numOfElements; i++) {
            for (int j = 1; j < (numOfElements - i); j++) {

                if (values.get(j - 1).compareTo(values.get(j)) > 0) {
                   T temp = values.get(j - 1);
                   values.set(j-1, values.get(j));
                   values.set(j, temp);
                }
            }
        }
    }
}