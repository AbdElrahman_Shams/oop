package eg.edu.alexu.csd.filestructure.sort.cs40;

import java.util.ArrayList;

import eg.edu.alexu.csd.filestructure.sort.IHeap;

public class MyHeapSort<T extends Comparable<T>> {
	
	private IHeap<T> heap ;
	
	public MyHeapSort() {
		
		heap = new MyHeap<T>();
		
	}
	
	public IHeap<T> heapSort(final ArrayList<T> values ) {
		
		heap.build(values);
		int size = heap.size();
		
		
		for(int i = size - 1 ; i >= 0; i-- ) {
			
			values.set(i, heap.extract());
			
		}
		
		return heap;
	}

}
