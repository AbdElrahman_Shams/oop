package eg.edu.alexu.csd.filestructure.sort.cs40;
import java.util.ArrayList;

//**********QuickSort**********//
public class SortFast <T extends Comparable<T>> {

  public void sort(ArrayList<T> values) {

      quickSort(values,0,values.size() - 1);
  }

  private void quickSort(ArrayList<T> unsorted,int low, int high) {
      int i = low, j = high;
      // Get the pivot element from the middle of the list
      T pivot = unsorted.get(low + (high-low)/2);

      // Divide into two lists
      while (i <= j) {
          while (pivot.compareTo(unsorted.get(i)) > 0) {
              i++;
          }
          while (unsorted.get(j).compareTo(pivot) > 0) {
              j--;
          }

          // As we are done we can increase i and j
          if (i <= j) {
              exchange(i, j, unsorted);
              i++;
              j--;
          }
      }
      // Recursion
      if (low < j)
          quickSort(unsorted,low, j);
      if (i < high)
          quickSort(unsorted,i, high);
  }

  private void exchange(int i, int j, ArrayList<T> unsorted) {
      T temp = unsorted.get(i);
      unsorted.set(i, unsorted.get(j));
      unsorted.set(j, temp);
  }
}