package eg.edu.alexu.csd.filestructure.sort.cs40;

import java.util.ArrayList;
import java.util.Collection;

import eg.edu.alexu.csd.filestructure.sort.IHeap;
import eg.edu.alexu.csd.filestructure.sort.INode;

public class MyHeap<T extends Comparable<T>> implements IHeap<T> {

	private ArrayList<INode<T>> myHeap = new ArrayList<INode<T>>();
	private int heapSize = 0;

	@Override
	public INode<T> getRoot() {
		if (heapSize > 0)
			return myHeap.get(0);
		return null;
	}

	@Override
	public int size() {
		return heapSize;
	}

	// this function swaps values of two nodes
	private void swap(INode<T> firstNode, INode<T> secondNode) {

		T temp = firstNode.getValue();

		firstNode.setValue(secondNode.getValue());
		secondNode.setValue(temp);

	}

	public void setHeapSize(int heapSize) {
		this.heapSize = heapSize;
	}

	@Override
	public void heapify(INode<T> node) {

		INode<T> leftChild = node.getLeftChild();
		INode<T> rightChild = node.getRightChild();

		INode<T> largest;

		if (leftChild != null && leftChild.getValue().compareTo(node.getValue()) > 0) {
			largest = leftChild;
		} else {
			largest = node;
		}
		if (rightChild != null && rightChild.getValue().compareTo(largest.getValue()) > 0) {
			largest = rightChild;
		}
		if (!largest.equals(node)) {
			swap(node, largest);
			heapify(largest);
		}

	}

	@Override
	public T extract() {
		if (heapSize <= 0)
			return null;

		T largest = getRoot().getValue();
		swap(getRoot(), myHeap.get(heapSize - 1));
		heapSize--;

		if (heapSize > 0) {
			heapify(getRoot());
		}

		return largest;
	}

	@Override
	public void insert(T element) {

		if (heapSize == myHeap.size())
			myHeap.add(new MyNode(element, heapSize));
		else
			myHeap.set(heapSize, new MyNode(element, heapSize));

		heapSize++;

		heapify(getRoot());
	}

	@Override
	public void build(Collection<T> unordered) {

		heapSize = unordered.size();
		int i = 0;

		for (T t : unordered) {
			myHeap.add(new MyNode(t, i));
			i++;
		}

		for (int j = (size() - 1) / 2; j >= 0; j--) {

			if (size() > 0)
				heapify(myHeap.get(j));

		}

	}

	private class MyNode implements INode<T> {

		private int index;
		private T value;

		MyNode(T value, int index) {

			this.index = index;
			this.value = value;
		}

		@Override
		public INode<T> getLeftChild() {
			int leftChildIndex = 2 * (index + 1) - 1;

			if (leftChildIndex < size() && leftChildIndex > 0)
				return myHeap.get(leftChildIndex);

			return null;
		}

		@Override
		public INode<T> getRightChild() {

			int rightChildIndex = (2 * (index + 1));

			if (rightChildIndex < size() && rightChildIndex > 0)
				return myHeap.get(rightChildIndex);

			return null;
		}

		@Override
		public INode<T> getParent() {

			int parentIndex = (index - 1) / 2;

			if (parentIndex < size() && parentIndex > 0)
				return myHeap.get(parentIndex);

			return null;
		}

		@Override
		public T getValue() {
			return value;
		}

		@Override
		public void setValue(T value) {
			this.value = value;
		}

	}
}
