package eg.edu.alexu.csd.oop.calculator.cs40;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import eg.edu.alexu.csd.oop.calculator.Calculator;

public class ICalculator implements Calculator {

	// some important instance variables
	private String[] input = null;
	private String operator = null;
	private String result = null;
	private String current = null;
	final private int historySize = 5;
	private String[] history = new String[historySize];
	private int index = -1;
	private String prev = null;
	private int prevIndex;
	private String next = null;
	private int nextIndex;
	private int currentIndex = -1;
	private int savedCurrentIndex = 0;

	@Override
	public void input(String s) {
		if (s != null) {

			if (s.contains("+")) {
				input = s.split("[+]");
				operator = "+";
			} else if (s.contains("-")) {
				input = s.split("[-]");
				operator = "-";
			} else if (s.contains("*")) {
				input = s.split("[*]");
				operator = "*";
			} else if (s.contains("/")) {
				input = s.split("[/]");
				operator = "/";
			} else {
				throw new RuntimeException();
			}
			// prevent adding the current again
			if (s != current) {
				if (index == -1) {
					++index;
				}
				if (historyIsFull()) {
					for (int i = 0; i < historySize - 1; ++i) {
						history[i] = history[i + 1];
					}
					--index;
				}
				StringBuilder sb = new StringBuilder();
				sb.append(input[0]).append(operator).append(input[1]);
				history[index] = sb.toString();
				current = history[index];
				currentIndex = index;
				++index;
			}
		} else {
			throw new RuntimeException();
		}
	}

	@Override
	public String getResult() {

		double a, b, r = 0;

		if (input != null) {

			input(current);

			a = Double.parseDouble(input[0]);
			b = Double.parseDouble(input[1]);

			if (operator.equals("+")) {
				r = a + b;
			} else if (operator.equals("-")) {
				r = a - b;
			} else if (operator.equals("*")) {
				r = a * b;
			} else if (operator.equals("/")) {
				r = a / b;
			}

			result = String.valueOf(r);
			return result;

		} else {
			// input is null
			throw new RuntimeException();
		}
	}

	// checking if the history is empty or not
	private boolean historyIsEmpty() {
		return (index == -1);
	}

	// checking if the history is full or not
	private boolean historyIsFull() {
		return (index == historySize);
	}

	// for testing
	protected void printHistory() {
		for (int i = 0; i < index; ++i) {
			System.out.println(history[i]);
		}
	}

	@Override
	public String current() {

		if (currentIndex == -1) {
			current = null;
		}
		return current;
	}

	@Override
	public String prev() {

		if (historyIsEmpty()) {
			prev = null;

		} else {
			// history is not empty

			if (currentIndex <= 0) {
				prev = null;
			} else {
				prevIndex = currentIndex - 1;
				prev = history[prevIndex];

				if (prev != null) {
					--currentIndex;
					current = prev;
				}
			}
		}

		return prev;
	}

	@Override
	public String next() {

		if (historyIsEmpty()) {
			next = null;
		} else {
			// there is no next operation
			if (currentIndex >= historySize - 1) {
				next = null;
			} else {
				nextIndex = currentIndex + 1;
				next = history[nextIndex];

				if (next != null) {
					++currentIndex;
					current = next;
				}
			}
		}

		return next;
	}

	@Override
	public void save() {
		try {
			PrintWriter printWriter = new PrintWriter(new FileWriter("history.txt"));

			for (int i = 0; i < index; ++i) {
				printWriter.println(history[i]);
			}

			printWriter.close(); // no need to flush, since close() does it anyway.

			savedCurrentIndex = currentIndex;

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void load() {

		try {

			for (int i = 0; i < historySize; ++i) {
				history[i] = null;
			}

			FileReader inputFile = new FileReader("history.txt");

			BufferedReader bufferReader = new BufferedReader(inputFile);

			String line;
			int i = 0;

			while ((line = bufferReader.readLine()) != null) {

				history[i] = line;
				++i;
			}
			index = i;
			currentIndex = savedCurrentIndex;
			current = history[currentIndex];
			bufferReader.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
